import Head from 'next/head';
import { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withRouter } from "next/router";
import {
  faInstagramSquare,
  faGoogle,
  faFacebookF,
  faPinterest
} from "@fortawesome/free-brands-svg-icons";
import { Image } from "antd";
class Register extends Component {
  state = {};

  register = () => {
    const { router, callback } = this.props;
    router.push(`/login`)
  }
  render() {
    const { router, callback } = this.props;

    return (
      <>
        <Head>
          <title>{"Register | RFDA"}</title>
          <meta property="og:title" content="My page title" key="title" />
          {/* <link rel="shortcut icon" href="/icon-web.png" /> */}
        </Head>
        <div className="flex items-center min-h-screen p-6 bg-gray-50 ">
          <div
            className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl"
          >
            <div className="flex flex-col overflow-y-auto md:flex-row">
              <div className="h-32 md:h-auto md:w-1/2 fullfit-image">
                <Image
                  className="object-cover w-full h-full"
                  preview={false}
                  src="https://i.pinimg.com/564x/5e/2d/41/5e2d415f06958bdcd9bafb1e89d53c08.jpg"
                  alt="Office"
                />
              </div>
              <div className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
                <div className="w-full">
                  <h1
                    className="mb-4 text-xl font-semibold text-gray-700 "
                  >
                    Register
                  </h1>
                  <label className="block text-sm">
                    <span className="text-gray-700 ">Name</span>
                    <input className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input" placeholder="Jane Doe" />
                  </label>
                  <label className="block mt-4 text-sm">
                    <span className="text-gray-700 ">Email</span>
                    <input className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input" placeholder="janedoe@gmail.com" />
                  </label>
                  <label className="block mt-4 text-sm">
                    <span className="text-gray-700 ">Password</span>
                    <input
                      className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input"
                      placeholder="***************"
                      type="password"
                    />
                  </label>
                  <label className="block mt-4 text-sm">
                    <span className="text-gray-700 ">Confirm Password</span>
                    <input
                      className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input"
                      placeholder="***************"
                      type="password"
                    />
                  </label>

                  <div
                    className="block w-full px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-orange-600 border border-transparent rounded-lg active:bg-orange-600 hover:bg-orange-700 focus:outline-none focus:shadow-outline-orange"
                    onClick={() => this.register()}
                  >
                    Sign Up
                  </div>

                  <hr className="my-8" />

                  <div
                    className="flex items-center justify-center w-full px-4 py-2 text-sm font-medium leading-5 text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray"
                  >
                    <FontAwesomeIcon icon={faGoogle} className="mr-2"></FontAwesomeIcon>
                    Google
                  </div>
                  <div
                    className="flex items-center justify-center w-full px-4 py-2 mt-4 text-sm font-medium leading-5 text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray"
                  >
                    <FontAwesomeIcon icon={faFacebookF} className="mr-2"></FontAwesomeIcon>
                    Facebook
                  </div>

                  <p className="mt-4">
                    <span
                      className="text-sm font-medium text-orange-600 hover:underline cursor-pointer"
                      onClick={() => {
                        router.push(`/login`);
                      }}
                    >
                      already have an account?
                    </span>
                  </p>

                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(Register);
