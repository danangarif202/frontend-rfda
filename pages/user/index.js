import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faClipboardList, faLock, faBell, faCircleUser, faMapLocationDot, faCheck } from "@fortawesome/free-solid-svg-icons";
import Navbar from "../../src/components/navbar";
import Footer from "../../src/components/footer";
import { Image } from "antd";

class User extends Component {
  state = {
    activeSearch: false,
    activeMenu: "Biodata Diri",
    activeAddress: 0
  };
  render() {
    const { router, dataAuth } = this.props;
    const { activeSearch, activeMenu, activeAddress } = this.state;
    const menuBar = [
      {
        label: "Biodata Diri",
        icon: faCircleUser,
      },
      {
        label: "Daftar Alamat",
        icon: faMapLocationDot,
      },
      {
        label: "Pembayaran",
        icon: faClipboardList,
      },
      {
        label: "Notifikasi",
        icon: faBell,
      },
      {
        label: "Keamanan",
        icon: faLock,
      },
    ]

    return (
      <>
        {dataAuth &&
          <div>
            {activeSearch && <div style={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }} className="w-full h-screen fixed"></div>}
            <Navbar callback={(e) => { this.setState({ activeSearch: e }) }} />
            <div className="container mx-auto mt-14 md:mt-20 py-4 md:py-8">
              <div className="md:mx-28">

                <div className="border mt-4 rounded-lg">
                  <div className="w-full border-b border-gray-200">
                    <ul className="flex flex-wrap -mb-px text-sm font-medium text-center text-gray-500 mt-1">
                      {menuBar.map((value, idx) => (
                        <li key={idx} className="mr-2" onClick={() => this.setState({ activeMenu: value.label })}>
                          <div className={`${activeMenu == value.label ? 'text-orange-700 border-orange-700 active' : 'hover:text-gray-600 hover:border-gray-300'} border-transparent inline-flex p-4 px-6 rounded-t-lg border-b-2 group cursor-pointer`}>
                            <FontAwesomeIcon
                              className={`${activeMenu == value.label ? 'text-orange-700 group-hover:text-orange-700' : 'text-gray-400 group-hover:text-gray-500'} mr-2 w-5 h-5`}
                              icon={value.icon}
                            ></FontAwesomeIcon>
                            {value.label}
                          </div>
                        </li>
                      ))}

                    </ul>
                  </div>

                  {/* content menu */}
                  {activeMenu == "Biodata Diri" ?
                    <div className="flex p-4">
                      <div className="w-3/12 pr-3">
                        <div style={{ boxShadow: "0 1px 6px 0 var(--color-shadow,rgba(49,53,59,0.12))" }} className="rounded-lg p-4">
                          <Image
                            className="rounded-lg w-full"
                            preview={false}
                            src={dataAuth?.data.imageProfile}
                            alt={"profile"}
                          />

                          <div className="border rounded-lg mt-3 text-center py-2 font-bold text-gray-700 text-sm">Pilih Foto</div>
                          <div className="mt-4 text-sm text-gray-500">Besar file: maksimum 10.000.000 bytes (10 Megabytes). Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</div>

                        </div>
                        <div className="border rounded-lg mt-8 text-center py-2 font-bold text-gray-700 text-sm">Ubah Kata Sandi</div>
                        <div className="border rounded-lg mt-2 text-center py-2 font-bold text-gray-700 text-sm">PIN RFDA</div>
                      </div>
                      <div className="w-9/12 pl-5">
                        <div className="font-bold text-gray-500 mb-3 mt-3">Ubah Biodata Diri</div>

                        <div className="flex mb-2">
                          <div className="w-2/12">Nama</div>
                          <div className="w-10/12">{dataAuth?.data.name}</div>
                        </div>
                        <div className="flex mb-2">
                          <div className="w-2/12">Tanggal Lahir</div>
                          <div className="w-10/12">{dataAuth?.data.dob}</div>
                        </div>
                        <div className="flex mb-2">
                          <div className="w-2/12">Jenis Kelamin</div>
                          <div className="w-10/12">{dataAuth?.data.gender}</div>
                        </div>

                        <div className="font-bold text-gray-500 mb-3 mt-8">Ubah Kontak</div>

                        <div className="flex mb-2">
                          <div className="w-2/12">Email</div>
                          <div className="w-10/12">
                            {dataAuth?.data.email}
                            {dataAuth?.data.isVerifiedEmail && <span className="bg-orange-300 text-sm px-2 py-0.5 rounded-lg mx-2 font-semibold text-orange-700">Terverifikasi</span>}
                            <span className="text-orange-700 ml-2 cursor-pointer">Ubah</span>
                          </div>
                        </div>
                        <div className="flex mb-2">
                          <div className="w-2/12">Nomor HP</div>
                          <div className="w-10/12">
                            {dataAuth?.data.phoneNumber}
                            {dataAuth?.data.isVerifiedPhoneNumber && <span className="bg-orange-300 text-sm px-2 py-0.5 rounded-lg mx-2 font-semibold text-orange-700">Terverifikasi</span>}
                            <span className="text-orange-700 ml-2 cursor-pointer">Ubah</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    : activeMenu == "Daftar Alamat" ?
                      <div className="p-4">
                        {["RUMAH", "KANTOR SHOESMART", "KAMPUS UNTAG"].map((value, idx) => (
                          <div
                            key={idx}
                            style={{ boxShadow: "0 1px 6px 0 var(--color-shadow,rgba(49,53,59,0.12))" }}
                            className={`${activeAddress == idx ? 'bg-orange-100 border border-orange-400' : 'border'} flex rounded-lg p-4 px-6 items-center mb-5`}
                          >
                            <div className="w-11/12">
                              <div className="font-bold">{value} {activeAddress == idx && <span className="border bg-gray-100 text-sm px-2 py-0.5 rounded-lg mx-2 font-semibold text-gray-400">Utama</span>}</div>
                              <div className="font-bold">Danang arif</div>

                              <div>6282210101997</div>
                              <div>Manyar Kartika I No.52, Manyar Kartika I, Kec. Sukolilo, Kota SBY, Jawa Timur, 60118</div>

                              <div className="text-sm my-4">Sudah Pinpoint</div>

                              <div className="flex text-sm font-bold text-orange-600">
                                <div className={`${activeAddress == idx ? '' : 'border-r-2'} pr-3`}>Ubah Alamat</div>
                                {activeAddress != idx &&
                                  <>
                                    <div className="border-r-2 px-3">Jadikan Alamat Utama & Pilih</div>
                                    <div className="pl-3">Hapus</div>
                                  </>
                                }
                              </div>
                            </div>
                            <div className="w-1/12 text-center">
                              {activeAddress == idx ?
                                <div className="text-right pr-3"><FontAwesomeIcon
                                  className="text-xl text-orange-600"
                                  icon={faCheck}
                                ></FontAwesomeIcon></div>
                                :
                                <div className="rounded-lg bg-orange-600 px-2 py-1 text-white cursor-pointer" onClick={() => this.setState({ activeAddress: idx })}>Pilih</div>
                              }
                            </div>
                          </div>
                        ))}
                      </div>
                      :
                      <div className="h-80"></div>
                  }
                </div>
              </div>
            </div>
            <Footer></Footer>
          </div>
        }
      </>
    );
  }
}

const mapState = (state) => ({
  dataAuth: state.auth.credential,
});

const mapDispatch = {
  // logout: authAction.logout,
};

export default withRouter(connect(mapState, mapDispatch)(User));