import Head from 'next/head';
import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import authAction from "../src/stores/auth/auth.action";

import { Image, Form, Input, Button, Checkbox } from 'antd';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleNotch } from "@fortawesome/free-solid-svg-icons";
import { faGoogle, faFacebookF } from "@fortawesome/free-brands-svg-icons";

class Login extends Component {
  state = {
    alertError: false,
    isLoading: false
  };

  onFinish = (values) => {
    const { login, router } = this.props;
    this.setState({ alertError: false, isLoading: true });

    login(values, (status) => {
      if (status) {
        this.setState({ isLoading: false });
        router.replace("/");
      } else {
        this.setState({ alertError: true, isLoading: false });
      }
    });
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  render() {
    const { router } = this.props;
    return (
      <>
        <Head>
          <title>{"Login | RFDA"}</title>
          <meta property="og:title" content="My page title" key="title" />
          {/* <link rel="shortcut icon" href="/icon-web.png" /> */}
        </Head>
        <div className="flex items-center min-h-screen p-6 bg-gray-50 ">
          <div
            className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl"
          >
            <div className="flex flex-col overflow-y-auto md:flex-row">
              <div className="h-32 md:h-auto md:w-1/2 fullfit-image">
                <Image
                  className="object-cover w-full h-full"
                  preview={false}
                  src="https://images.unsplash.com/photo-1553095066-5014bc7b7f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8d2FsbCUyMGJhY2tncm91bmR8ZW58MHx8MHx8&w=1000&q=80"
                  alt="Office"
                />
              </div>
              <div className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
                <div className="w-full">
                  <h1
                    className="mb-4 text-xl font-semibold text-gray-700 "
                  >
                    Login
                  </h1>

                  <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={this.onFinish}
                    onFinishFailed={this.onFinishFailed}
                    autoComplete="off"
                  >
                    <Form.Item
                      label="Email"
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your username!',
                        },
                      ]}
                    >
                      <Input
                        type="email"
                        placeholder="user@gmail.com"
                        className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input"
                      />
                    </Form.Item>

                    <Form.Item
                      label="Password"
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                      ]}
                      className="mt-2"
                    >
                      <Input
                        type="password"
                        className="block w-full mt-1 text-sm focus:border-orange-400 focus:outline-none focus:shadow-outline-orange form-input"
                        placeholder="Password"
                      />
                    </Form.Item>

                    <Form.Item
                      wrapperCol={{
                        offset: 8,
                        span: 16,
                      }}
                    >
                      <Button type="primary" htmlType="submit" className="block w-full px-4 py-2 mt-4 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-orange-600 border border-transparent rounded-lg active:bg-orange-600 hover:bg-orange-700 focus:outline-none focus:shadow-outline-orange">
                        Log in
                      </Button>
                    </Form.Item>
                  </Form>

                  {this.state.alertError && <div className="text-sm mt-3 text-red-600">E-mail atau password yang Anda masukkan salah, mohon periksa kembali.</div>}
                  {this.state.isLoading && <div className="text-center text-lg mt-3 text-blue-700">
                    <FontAwesomeIcon icon={faCircleNotch} className="animate-spin"></FontAwesomeIcon>
                  </div>}
                  <hr className="my-8" />


                  <div
                    className="flex items-center justify-center w-full px-4 py-2 text-sm font-medium leading-5 text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray"
                  >
                    <FontAwesomeIcon icon={faGoogle} className="mr-2"></FontAwesomeIcon>
                    Google
                  </div>
                  <div
                    className="flex items-center justify-center w-full px-4 py-2 mt-4 text-sm font-medium leading-5 text-gray-700 transition-colors duration-150 border border-gray-300 rounded-lg active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray"
                  >
                    <FontAwesomeIcon icon={faFacebookF} className="mr-2"></FontAwesomeIcon>
                    Facebook
                  </div>

                  <p className="mt-4">
                    <span
                      className="text-sm font-medium text-orange-600 hover:underline cursor-pointer"
                    >
                      Forgot your password?
                    </span>
                  </p>
                  <p className="mt-1">
                    <span
                      className="text-sm font-medium text-orange-600 hover:underline cursor-pointer"
                      onClick={() => {
                        router.push(`/register`);
                      }}
                    >
                      Create account
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

// export default withRouter(Login);

const mapState = (state) => ({});
const mapDispatch = {
  login: authAction.login,
};

export default withRouter(connect(mapState, mapDispatch)(Login));