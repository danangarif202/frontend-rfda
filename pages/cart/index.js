import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import axios from "axios";
import storageKey from "../../src/key/storage.key";
import storagePlugin from "../../src/plugins/storage.plugin";

import { Image, Alert } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faChevronRight, faStar, faEllipsis, faCircleNotch, faCirclePlus, faCircleMinus, faTrashCan, faXmark } from "@fortawesome/free-solid-svg-icons";
import CurrencyFormat from "react-currency-format";
import Navbar from "../../src/components/navbar";
import Footer from "../../src/components/footer";

class Cart extends Component {
    state = {
        activeSearch: false,
        cart: [],
        cartSelected: []
    };

    componentDidMount() {
        const { router } = this.props;
        const isUser = storagePlugin.isExist(storageKey.storeKey);
        if (!isUser) {
            router.push(`login`);
        } else {
            this.getCart();
        }
    }

    getCart() {
        const dummyData = [
            {
                id: 2,
                name: "Celana Pendek Waffa Tali Bahan Babyterry - Salem BBTR, All Size",
                price: "53000",
                count: 1,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(9).jpg"
            },
            {
                id: 4,
                name: "Dailyoutfit Exclusive Kaos Zodiac Scorpio Unisex Premium Quality - M, Hitam",
                price: "75000",
                count: 3,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(11).jpg"
            },
            {
                id: 3,
                name: "Dailyoutfits T-Shirt Lhotse Black - Hitam, M",
                price: "94000",
                count: 1,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(12).jpg"
            },
            {
                id: 5,
                name: "Kaos Unisex Erigo T-Shirt Custombike Cotton Combed Navy - S",
                price: "62400",
                count: 6,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(7).jpg"
            },
        ]

        this.setState({ cart: dummyData })
    };

    submitCart = () => {
        const { router } = this.props;
        const { activeSearch, cartSelected } = this.state;
        router.push(`checkout`);
    };

    render() {
        const { cart, activeSearch, cartSelected } = this.state;

        let totalItem = 0;
        let totalPrice = 0;
        cartSelected.map((e) => {
            totalItem += parseInt(e.count);
            totalPrice += parseInt(e.price);
        })
        let statusCheckedAll = cart.length === cartSelected.length;

        return (
            <>
                <div>
                    {activeSearch && <div style={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }} className="w-full h-screen fixed"></div>}
                    <Navbar callback={(e) => { this.setState({ activeSearch: e }) }} />
                    <div className="container mx-auto mt-20 py-4 md:py-8 px-28">
                        <div className="flex">
                            <div className="w-9/12">
                                <div className="border-b-4 flex">
                                    <div className="inline-flex items-center">
                                        <input
                                            type="checkbox"
                                            className="mr-2 mb-4 accent-orange-600 w-5 h-5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 cursor-pointer"
                                            checked={statusCheckedAll}
                                            onChange={() => { }}
                                            onClick={() => {
                                                let selectAll = []
                                                if (cartSelected.length < cart.length) {
                                                    cart.map((e) => { selectAll.push(e) })
                                                }
                                                this.setState({ cartSelected: selectAll });
                                            }}
                                        />
                                    </div>
                                    <div>
                                        Pilih Semua
                                    </div>
                                </div>
                                <div className="font-bold mt-3 text-gray-600">Toko Erigo Store</div>
                                <div className="text-sm text-gray-500 mt-1">Jakarta Pusat</div>
                                {cart.map((value, idx) => {
                                    const status = cartSelected.map((e) => { return e.id }).indexOf(value.id) > -1
                                    return (
                                        <div className="border-b-2 pt-2 pb-4" key={idx}>
                                            <div className="flex my-3">
                                                <div className="inline-flex items-center">
                                                    <input
                                                        type="checkbox"
                                                        className="mr-4 accent-orange-600 w-5 h-5 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 cursor-pointer"
                                                        checked={status}
                                                        onChange={() => { }}
                                                        onClick={() => {

                                                            let stateCart = this.state.cartSelected
                                                            let indexValue = stateCart.map((e) => { return e.name }).indexOf(value.name);

                                                            if (indexValue !== -1) {
                                                                stateCart.splice(indexValue, 1);
                                                            } else {
                                                                stateCart.push(value)
                                                            }

                                                            let paramCart = []
                                                            stateCart.map((e) => { paramCart.push(e) })

                                                            if (paramCart.length < 1) {
                                                                paramCart = []
                                                            }

                                                            this.setState({
                                                                cartSelected: paramCart,
                                                            });
                                                        }}
                                                    />
                                                </div>
                                                <div className="w-fit cursor-pointer">
                                                    <Image
                                                        className="rounded-lg w-24"
                                                        preview={false}
                                                        src={value.image}
                                                        alt={"sss"}
                                                    />
                                                </div>
                                                <div className="w-9/12 px-3">
                                                    <div className="mb-2 cursor-pointer">{value.name}</div>
                                                    <div className="font-bold text-gray-600">
                                                        <CurrencyFormat
                                                            value={value.price}
                                                            displayType={"text"}
                                                            thousandSeparator={'.'}
                                                            decimalSeparator={','}
                                                            prefix={"Rp "}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex justify-between items-center">
                                                <div>hitam Ubah</div>
                                                <div className="flex items-center">
                                                    <div className="text-sm text-gray-600 mr-3 pr-3 border-r-2 cursor-pointer">Pindahkan ke Wishlist</div>
                                                    <div className="flex text-gray-500">
                                                        <div className="mx-3 text-lg mr-12 cursor-pointer"><FontAwesomeIcon icon={faTrashCan}></FontAwesomeIcon></div>
                                                        <div className={value.count > 1 ? 'text-orange-600 cursor-pointer ml-3 mr-1 text-lg' : `ml-3 mr-1 text-lg cursor-pointer`}><FontAwesomeIcon icon={faCircleMinus}></FontAwesomeIcon></div>
                                                        <div className="px-5 border-b">{value.count}</div>
                                                        <div className={value.count >= 1 ? 'text-orange-600 cursor-pointer mr-1 text-lg' : `mr-3 ml-1 text-lg cursor-pointer`}><FontAwesomeIcon icon={faCirclePlus}></FontAwesomeIcon></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="w-3/12">
                                <div className="border rounded-lg ml-6 my-4">
                                    <div className="p-4 border-b-4">
                                        <div className="p-3 border rounded-lg text-gray-500">
                                            <div className="font-bold">Makin hemat pakai promo</div>
                                            <div className="text-sm">Pilih barang dulu sebelum pakai promo</div>
                                        </div>
                                    </div>

                                    <div className="p-4">
                                        <div className="font-bold text-gray-600">Ringkasan belanja</div>
                                        <div className="flex justify-between my-3 pb-4 border-b text-gray-500">
                                            <div>Total Harga ({totalItem} barang)</div>
                                            <div>
                                                <CurrencyFormat
                                                    value={totalPrice}
                                                    displayType={"text"}
                                                    thousandSeparator={'.'}
                                                    decimalSeparator={','}
                                                    prefix={"Rp "}
                                                />
                                            </div>
                                        </div>

                                        <div>
                                            <div className="flex justify-between my-3 font-bold text-gray-600">
                                                <div>Total Harga</div>
                                                <div>
                                                    {
                                                        totalPrice > 0 ?
                                                            <CurrencyFormat
                                                                value={totalPrice}
                                                                displayType={"text"}
                                                                thousandSeparator={'.'}
                                                                decimalSeparator={','}
                                                                prefix={"Rp "}
                                                            />
                                                            :
                                                            <>-</>
                                                    }
                                                </div>
                                            </div>

                                            <div
                                                style={totalItem > 0 ? { backgroundColor: "#03AC0E" } : {}}
                                                className={`${totalItem > 0 ? 'text-white cursor-pointer' : 'bg-gray-300 text-gray-500 cursor-not-allowed'} py-2 font-bold border text-center rounded-lg my-2`}
                                                onClick={() => {
                                                    if (totalItem > 0) {
                                                        this.submitCart();
                                                    }
                                                }}
                                            >
                                                Beli ({totalItem})
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer></Footer>
                </div>
            </>

        );
    }
}

export default withRouter(Cart);
