import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import axios from "axios";
import productAction from "../../src/stores/product/product.action";

import { Image } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faStar, faEllipsis, faCircleNotch, faFilter, faXmark } from "@fortawesome/free-solid-svg-icons";
import CurrencyFormat from "react-currency-format";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import Navbar from "../../src/components/navbar";
import Footer from "../../src/components/footer";

class Product extends Component {
  constructor(props) {
    super(props);
    this.priceMin = React.createRef();
    this.priceMax = React.createRef();
  }
  state = {
    product: [],
    category: [],
    delivery: [],
    dataFilter: {
      page: 1,
      pageCategory: 1,
      pageDelivery: 1,
      min: null,
      max: null,
      category: [],
      paramCategory: [],
      delivery: {},
      order: "Paling Sesuai",
      paramOrder: null
    },
    pagination: 0,
    paginationCategory: 0,
    paginationDelivery: 0,
    totalData: 0,
    activeDwopdown: false,
    activeFilterCategory: false,
    activeFilterDelivery: false,
    activeFilterPrice: false,
    activeSearch: false,
    isLoading: false,
    loadingImage: false,
    buttonBackToTop: false,
    filterMobile: false,
    flagButtonFilter: false
  };

  componentDidMount() {
    // console.log("from componentDidMount")
    this.initialData();
    document.addEventListener("scroll", this.onScroll);
    document
      .querySelector("#scrollerCategory")
      .addEventListener("scroll", this.onScrollFilter);
    document
      .querySelector("#scrollerDelivery")
      .addEventListener("scroll", this.onScrollFilter);
  }

  initialData = () => {
    this.getProduct();
    this.getCategory();
    this.getDelivery();
  }

  onScroll = (event) => {
    const { browseCatalog, pagination, filterProduct, resetProduct } = this.props;
    // console.log("window", window)
    // console.log("target", event.target.body)
    // console.log("pageYOffset", window.pageYOffset)
    // console.log("scrollHeight", document.body.scrollHeight)
    // console.log("scrollY", window.scrollY)
    // console.log("height", window.screen.height)
    // console.log("height", document.body.offsetHeight)
    // console.log("clientHeight", document.body.clientHeight)

    // =========================
    // ====== BACK TO TOP ======
    // =========================
    if (window.pageYOffset > 300) {
      this.setState({ buttonBackToTop: true });
    } else {
      this.setState({ buttonBackToTop: false });
    }

    // ====== Param Category ======
    let paramCategory = []
    this.state.dataFilter.category.map((e) => { paramCategory.push(e.id) })
    if (paramCategory.length < 1) {
      paramCategory = null
    }

    // ====== Param Order ======
    const valOrder = this.state.dataFilter.order
    let orderValue = null
    if (valOrder == "Terbaru") {
      orderValue = "new"
    } else if (valOrder == "Harga Tertinggi") {
      orderValue = "high"
    } else if (valOrder == "Harga Terendah") {
      orderValue = "low"
    }

    // ====== Param Delivery ======
    const delivery = this.state.dataFilter.delivery
    let paramDelivery = null
    if (delivery == {}) {
      paramDelivery = delivery.id
    }


    let userScrollHeight = window.innerHeight + window.scrollY;
    let windowBottomHeight = document.documentElement.offsetHeight;

    // if ((window.innerHeight + window.scrollY) >= window.screen.height) {
    // if (clientHeight - window.scrollY == 1181) {
    // if (clientHeight - window.scrollY < 1182 && clientHeight - window.scrollY > 1000) {

    if (userScrollHeight >= windowBottomHeight) {
      // console.log("page", this.state.dataFilter.page)
      // console.log("pagination", this.state.pagination.length)

      if (this.state.dataFilter.page < this.state.pagination.length) {

        this.setState({
          dataFilter: {
            ...this.state.dataFilter,
            page: this.state.dataFilter.page + 1
          },
        })

        const payload = {
          category: this.state.dataFilter.paramCategory,
          delivery: paramDelivery,
          order: orderValue,
          min: this.state.dataFilter.min,
          max: this.state.dataFilter.max,
          page: this.state.dataFilter.page + 1,
        }
        this.getProduct(payload)
      }

    }
  };

  onScrollFilter = (e) => {
    if (e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight) {

      if (e.target.id === "scrollerCategory") {
        if (this.state.dataFilter.pageCategory < this.state.paginationCategory.length) {
          this.setState({
            dataFilter: {
              ...this.state.dataFilter,
              pageCategory: this.state.dataFilter.pageCategory + 1
            },
          })
          this.getCategory(this.state.dataFilter.pageCategory + 1);
        }
      } else if (e.target.id === "scrollerDelivery") {
        if (this.state.dataFilter.pageDelivery < this.state.paginationDelivery.length) {
          this.setState({
            dataFilter: {
              ...this.state.dataFilter,
              pageDelivery: this.state.dataFilter.pageDelivery + 1
            },
          })
          this.getDelivery(this.state.dataFilter.pageDelivery + 1);
        }
      }
    }
  };

  scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  getCategory = async (page = 1) => {
    const paramPage = page > 1 ? `&page=${page}` : "";
    axios
      .get(`${process.env.NEXT_PUBLIC_MY_BASE_URL}/category?perPage=8${paramPage}`)
      .then((res) => {
        this.setState({
          category: [...this.state.category, ...res.data.data],
        });

        const countData = Math.ceil(res.data.total_data / 8);
        let dataPagination = [];

        for (let i = 0; i < countData; i++) {
          dataPagination.push(i);
        }
        this.setState({
          paginationCategory: dataPagination,
        })
      })
      .catch((error) => {
        alert(error);
      });
  };

  getDelivery = async (page = 1) => {
    const paramPage = page > 1 ? `&page=${page}` : "";
    axios
      .get(`${process.env.NEXT_PUBLIC_MY_BASE_URL}/delivery?perPage=8${paramPage}`)
      .then((res) => {
        this.setState({
          delivery: [...this.state.delivery, ...res.data.data],
        });

        const countData = Math.ceil(res.data.total_data / 8);
        let dataPagination = [];

        for (let i = 0; i < countData; i++) {
          dataPagination.push(i);
        }
        this.setState({
          paginationDelivery: dataPagination,
        })
      })
      .catch((error) => {
        alert(error);
      });
  };

  getProduct(payload) {

    /* const { browseCatalog, pagination, filterProduct, resetProduct } = this.props;
    const param = { "perPage": 15 }
    payload?.category != undefined && this.state.dataFilter.category.length > 0 ? param.category = `[${payload.category}]` : "";
    payload?.delivery != undefined ? param.delivery = payload.delivery : "";
    payload?.page != undefined ? param.page = payload.page : "";
    payload?.order != undefined && payload?.order != null ? param.order = payload.order : "";
    payload?.min != undefined && payload?.min != null ? param.min = payload.min : "";
    payload?.max != undefined && payload?.max != null ? param.max = payload.order : "";
    browseCatalog(param, () => { console.log("success") }, (err) => { console.log("error") }); */

    const paramCategory = payload?.category != undefined && this.state.dataFilter.category.length > 0 ? `&category=[${payload.category}]` : "";
    const paramDelivery = payload?.delivery != undefined ? `&delivery=${payload.delivery}` : "";
    const paramPage = payload?.page != undefined ? `&page=${payload.page}` : "";
    const paramOrder = payload?.order != undefined && payload?.order != null ? `&order=${payload.order}` : "";
    const paramMin = payload?.min != undefined && payload?.min != null ? `&priceMin=${payload.min}` : "";
    const paramMax = payload?.max != undefined && payload?.max != null ? `&priceMax=${payload.max}` : "";

    this.setState({ isLoading: true })
    axios
      .get(
        `${process.env.NEXT_PUBLIC_MY_BASE_URL}/products?perPage=15${paramPage}${paramCategory}${paramDelivery}${paramOrder}${paramMin}${paramMax}`
      )
      .then((res) => {
        const countData = Math.ceil(res.data.total_data / 15);
        let dataPagination = [];

        for (let i = 0; i < countData; i++) {
          dataPagination.push(i);
        }
        this.setState({
          product: [...this.state.product, ...res.data.data],
          pagination: dataPagination,
          totalData: res.data.total_data,
          isLoading: false
        })
      })
      .catch((error) => {
        alert(error);
        this.setState({ isLoading: false })
      });
  }

  setOrderProduct = (val) => {

    let orderValue = null
    if (val == "Terbaru") {
      orderValue = "new"
    } else if (val == "Harga Tertinggi") {
      orderValue = "high"
    } else if (val == "Harga Terendah") {
      orderValue = "low"
    }

    this.setState({
      dataFilter: {
        ...this.state.dataFilter,
        order: val,
        paramOrder: orderValue
      },
      product: [],
      activeDwopdown: !this.state.activeDwopdown
    })

    const payload = {
      category: this.state.dataFilter.paramCategory,
      delivery: this.state.dataFilter.delivery.id,
      order: orderValue,
      min: this.state.dataFilter.min,
      max: this.state.dataFilter.max,
      page: this.state.dataFilter.page,
    }

    this.getProduct(payload);
  }

  setFilterCategory = (val) => {
    let dataFilter = this.state.dataFilter.category
    let indexValue = dataFilter.map((e) => { return e.name }).indexOf(val.name);

    if (indexValue !== -1) {
      dataFilter.splice(indexValue, 1);
    } else {
      dataFilter.push(val)
    }

    let paramCategory = []
    dataFilter.map((e) => { paramCategory.push(e.id) })

    if (paramCategory.length < 1) {
      paramCategory = null
    }

    this.setState({
      dataFilter: {
        ...this.state.dataFilter,
        category: dataFilter,
        paramCategory: paramCategory,
        page: 1,
      },
      product: [],
    });

    const payload = {
      category: paramCategory,
      delivery: this.state.dataFilter.delivery.id,
      order: this.state.dataFilter.paramOrder,
      min: this.state.dataFilter.min,
      max: this.state.dataFilter.max,
    }

    this.getProduct(payload);
  }

  render() {
    const { router } = this.props;
    const {
      product,
      category,
      dataFilter,
      delivery,
      activeDwopdown,
      activeFilterCategory,
      activeFilterDelivery,
      activeFilterPrice,
      activeSearch,
      isLoading,
      loadingImage,
      buttonBackToTop,
      filterMobile,
      flagButtonFilter
    } = this.state;

    // const { listProduct, filterProduct, pagination, totalData } = this.props;

    return (
      <>
        <div>
          {activeSearch && <div style={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }} className="w-full h-screen fixed z-50"></div>}
          <Navbar callback={(e) => { this.setState({ activeSearch: e }) }} />
          <div className="container mx-auto mt-12 md:mt-20 py-4 md:py-8">
            <div className="hidden md:block">
              <div className="mx-28 flex justify-between pl-2">
                <div className="w-2/12">
                  <div className="text-black text-lg text-center font-bold inline-flex items-end">
                    Filter
                  </div>
                </div>
                <div className="w-10/12">
                  <div className="flex justify-between pl-6">

                    <div className="text-sm text-gray-600 inline-flex items-center">
                      Menampilkan 1 - {product.length} produk dari total {this.state.totalData}
                    </div>
                    <div>
                      <div className="mr-2 py-auto inline-flex items-center">Urutkan:</div>
                      <div
                        className={`${activeDwopdown ? 'rounded-t-lg' : 'rounded-lg'} cursor-pointer border w-44 justify-between text-gray-600 bg-white hover:bg-gray-100 hover:border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-sm px-4 py-2.5 text-center inline-flex items-center`}
                        onClick={() => {
                          this.setState({ activeDwopdown: !activeDwopdown })
                        }}
                      >
                        {dataFilter.order}
                        <FontAwesomeIcon icon={!activeDwopdown ? faChevronDown : faChevronUp}></FontAwesomeIcon>
                      </div>
                      <div className="flex justify-end">
                        <div className={`${!activeDwopdown ? 'hidden' : ''} border cursor-pointer z-10 w-44 bg-white rounded-b-lg divide-y divide-gray-100 shadow absolute`}>
                          <ul className="py-1 text-sm text-gray-600">
                            {["Terbaru", "Harga Tertinggi", "Harga Terendah", "Ulasan", "Paling Sesuai"].map((value, idx) => (
                              <li
                                key={idx}
                                onClick={() => {
                                  this.setOrderProduct(value);
                                }}
                              >
                                <div className={`${dataFilter.order == value ? 'border-l-4 border-x-orange-700' : ''} block py-2 px-4 hover:bg-gray-100`}>{value}</div>
                              </li>
                            ))}

                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="md:mx-28 flex flex-wrap">
              <div className="hidden md:block w-full lg:w-2/12 xl:w-2/12 pl-2 pt-2">

                <div className="shadow-md rounded-lg">
                  <div className="p-4 pr-3 border-b" >
                    <div className={`${!activeFilterCategory ? 'mb-2' : ''} font-bold text-left flex justify-between`}>
                      <div className="text-gray-600 text-sm">Kategory</div>
                      <div
                        onClick={() => {
                          this.setState({ activeFilterCategory: !activeFilterCategory })
                        }}
                        className="text-gray-600 text-sm"
                      >
                        <FontAwesomeIcon className="mr-1" icon={activeFilterCategory ? faChevronDown : faChevronUp}></FontAwesomeIcon>
                      </div>
                    </div>
                    <div className="max-h-36 overflow-y-auto" id="scrollerCategory">
                      {!activeFilterCategory && category.map((val, index) => {
                        const status = dataFilter.category.map((e) => { return e.name }).indexOf(val.name) > -1
                        return (
                          <div
                            className="text-black text-left cursor-pointer"
                            key={index}
                            onClick={() => this.setFilterCategory(val)}
                          >
                            <input
                              type="checkbox"
                              className="mr-2 accent-orange-600 w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 cursor-pointer"
                              checked={status}
                              onChange={() => { }}
                            />
                            <span
                              className={
                                status
                                  ? "text-orange-500 font-bold"
                                  : null
                              }
                            >
                              {val.name}
                            </span>
                          </div>
                        )
                      })
                      }
                    </div>
                  </div>

                  <div className="p-4 pr-3 border-b">
                    <div className={`${!activeFilterDelivery ? 'mb-2' : ''} font-bold text-left flex justify-between`}>
                      <div className="text-gray-600 text-sm">Pengiriman</div>
                      <div
                        onClick={() => {
                          this.setState({ activeFilterDelivery: !activeFilterDelivery })
                        }}
                        className="text-gray-600 text-sm"
                      >
                        <FontAwesomeIcon className="mr-1" icon={activeFilterDelivery ? faChevronDown : faChevronUp}></FontAwesomeIcon>
                      </div>
                    </div>
                    <div className="max-h-36 overflow-y-auto" id="scrollerDelivery">
                      {!activeFilterDelivery && delivery.map((valueDelivery, index) => {
                        return (
                          <div
                            className="text-black text-left cursor-pointer"
                            key={index}
                            onClick={() => {

                              let initValueDelivery = {}
                              if (this.state.dataFilter.delivery != {}) {
                                if (this.state.dataFilter.delivery.name != valueDelivery.name) {
                                  initValueDelivery = valueDelivery
                                }
                              }

                              this.setState({
                                dataFilter: {
                                  ...this.state.dataFilter,
                                  delivery: initValueDelivery,
                                  page: 1
                                },
                                product: []
                              });

                              if (initValueDelivery.id == undefined) {
                                initValueDelivery = null
                              } else {
                                initValueDelivery = initValueDelivery.id
                              }

                              const payload = {
                                category: this.state.dataFilter.paramCategory,
                                delivery: initValueDelivery,
                                order: this.state.dataFilter.paramOrder,
                                min: this.state.dataFilter.min,
                                max: this.state.dataFilter.max,
                              }

                              this.getProduct(payload)

                            }}
                          >
                            <input
                              type="checkbox"
                              className="mr-2 accent-orange-600 w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 cursor-pointer"
                              checked={
                                dataFilter.delivery.name === valueDelivery.name
                                  ? true
                                  : false
                              }
                              onChange={() => { }}
                            />
                            <span
                              className={
                                dataFilter.delivery.name === valueDelivery.name
                                  ? "text-orange-500 font-bold"
                                  : null
                              }
                            >
                              {valueDelivery.name}
                            </span>
                          </div>
                        )
                      })
                      }
                    </div>
                  </div>
                  <div className="p-4">
                    <div className={`${!activeFilterPrice ? 'mb-2' : ''} font-bold text-left flex justify-between`}>
                      <div className="text-gray-600 text-sm">Harga</div>
                      <div
                        onClick={() => {
                          this.setState({ activeFilterPrice: !activeFilterPrice })
                        }}
                        className="text-gray-600 text-sm"
                      >
                        <FontAwesomeIcon icon={activeFilterPrice ? faChevronDown : faChevronUp}></FontAwesomeIcon>
                      </div>
                    </div>
                    {!activeFilterPrice &&
                      <>
                        <div className="flex mt-3">
                          <span className="inline-flex items-center px-3 text-sm text-gray-500 font-bold bg-gray-100 rounded-l-md border border-r-0 border-gray-300">
                            Rp
                          </span>
                          <input
                            type="number"
                            ref={this.priceMin}
                            className="hide-arrows rounded-none rounded-r-lg bg-white border text-gray-900 focus:ring-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 focus:border-orange-400 focus:outline-none"
                            placeholder="Harga Minimum"
                            onBlur={(e) => {
                              // if (e.key == "Enter") {}
                              if (parseInt(e.target.value) > 0) {
                                this.setState({
                                  dataFilter: {
                                    ...this.state.dataFilter,
                                    min: parseInt(e.target.value)
                                  },
                                  product: []
                                });

                                const payload = {
                                  category: this.state.dataFilter.paramCategory,
                                  delivery: this.state.dataFilter.delivery.id,
                                  order: this.state.dataFilter.paramOrder,
                                  min: parseInt(e.target.value),
                                  max: this.state.dataFilter.max,
                                }

                                this.getProduct(payload)
                              }
                            }}
                          />
                        </div>
                        <div className="flex mt-3">
                          <span className="inline-flex items-center px-3 text-sm text-gray-500 font-bold bg-gray-100 rounded-l-md border border-r-0 border-gray-300">
                            Rp
                          </span>
                          <input
                            type="number"
                            ref={this.priceMax}
                            className="hide-arrows rounded-none rounded-r-lg bg-white border text-gray-900 focus:ring-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 focus:border-orange-400 focus:outline-none"
                            placeholder="Harga Maksimum"
                            onBlur={(e) => {
                              if (parseInt(e.target.value) > 0) {
                                this.setState({
                                  dataFilter: {
                                    ...this.state.dataFilter,
                                    max: parseInt(e.target.value)
                                  },
                                  product: []
                                });

                                const payload = {
                                  category: this.state.dataFilter.paramCategory,
                                  delivery: this.state.dataFilter.delivery.id,
                                  order: this.state.dataFilter.paramOrder,
                                  min: this.state.dataFilter.min,
                                  max: parseInt(e.target.value),
                                }

                                this.getProduct(payload)
                              }
                            }}
                          />
                        </div>
                        <div className="mt-3 w-full">
                          {[
                            "Rp38 rb - Rp99 rb",
                            "Rp120 rb - Rp250 rb",
                            "Rp350 rb - Rp400 rb",
                          ].map((list, i) => (
                            <div
                              key={i}
                              className="w-fit"
                            >
                              <div className="p-2 px-4 mb-3 border rounded-full text-sm text-gray-500">{list}</div>
                            </div>
                          ))}
                        </div>
                      </>
                    }
                  </div>

                </div>
                <div
                  className="p-3 border text-gray-600 font-semibold mt-5 rounded-lg text-center hover:bg-orange-700 hover:text-white"
                  onClick={() => {
                    this.setState({
                      dataFilter: {
                        page: 1,
                        min: null,
                        max: null,
                        category: [],
                        paramCategory: [],
                        delivery: {},
                        order: "Paling Sesuai"
                      },
                      product: [],
                      pagination: 0
                    })

                    this.priceMin.current.value = null
                    this.priceMax.current.value = null

                    this.getProduct()
                  }}
                >Reset Filter</div>
              </div>

              <div className="sm:hidden w-full">
                <div className="flex items-center text-gray-600 overflow-hidden">
                  <div
                    className="w-fit"
                    onClick={() => {
                      this.setState({ filterMobile: true })
                    }}
                  >
                    <div className="w-fit flex items-center border ml-3 mr-2 py-1 rounded-full px-4"><FontAwesomeIcon icon={faFilter} className="mr-1"></FontAwesomeIcon> filter</div>
                  </div>
                  <div className="w-9/12">

                    <Swiper
                      slidesPerView={"auto"}
                      style={{ paddingTop: "10px", paddingBottom: "10px" }}
                      onSlideChange={() => ""}
                      onSwiper={(swiper) => ""}
                      className="mySwiper sm:hidden"
                    >
                      {category.map((value, idx) => {
                        const status = dataFilter.category.map((e) => { return e.name }).indexOf(value.name) > -1
                        return (
                          <SwiperSlide className="width-auto" key={idx}>
                            <div
                              // className={`border mr-2 py-1 rounded-full px-4`}
                              className={
                                status
                                  ? "border border-orange-600 rounded-full mr-2 px-4 py-1 text-orange-600"
                                  : "border mr-2 py-1 rounded-full px-4"
                              }
                              onClick={() => this.setFilterCategory(value)}
                            >{value.name}</div>
                          </SwiperSlide>
                        )
                      })}
                    </Swiper>
                  </div>
                </div>
              </div>

              <div className="w-full lg:w-10/12 xl:w-10/12 px-2 md:pl-6 md:pr-0">

                <div className="flex flex-wrap lg:-mx-2 xl:-mx-2">
                  {product.map((product, index) => (
                    <div
                      key={index}
                      className="justify-center p-1.5 md:py-0 lg:my-2 lg:px-2 xl:my-2 xl:px-2 w-1/2 md:w-1/5 lg:w-1/5 xl:w-1/5"
                    >
                      <div
                        className="rounded-lg shadow-md md:shadow-lg bg-white max-w-sm h-full cursor-pointer"
                        onClick={() => {
                          router.push(`/product/${product.code}`);
                        }}
                      >
                        {/* <div className="text-center bg-orange-400 rounded-t-lg text-white text-2xl">
                          {
                            loadingImage ?
                              <div className="my-20 inline-flex items-center animate-spin">
                                <FontAwesomeIcon icon={faCircleNotch}></FontAwesomeIcon>
                              </div>
                              :
                              <Image
                                className="rounded-t-lg w-full"
                                onLoadStart={() => {
                                  this.setState({ loadingImage: true })
                                }}
                                onLoadEnd={() => {
                                  this.setState({ loadingImage: false })
                                }}
                                preview={false}
                                src={
                                  `${process.env.NEXT_PUBLIC_MY_BASE_URL}/` +
                                  product.image
                                }
                                alt={product.name}
                              />
                          }
                        </div> */}
                        <Image
                          className="rounded-t-lg w-full"
                          preview={false}
                          src={
                            `${process.env.NEXT_PUBLIC_MY_BASE_URL}/` +
                            product.image
                          }
                          alt={product.name}
                        />
                        <div className="p-4">
                          <h5 className="text-gray-900 text-sm font-medium mb-2">
                            {product.title}
                          </h5>

                          {product.selling_price > 0 ? (
                            <>
                              <div className="flex mb-1">
                                <div style={{ color: "#F94D63", backgroundColor: "#FFDBE2" }} className="text-sm font-bold px-2 mr-2 rounded-lg text-center">
                                  {(
                                    (product.selling_price / product.price) *
                                    100
                                  ).toFixed(0)}
                                  %
                                </div>
                                <div className="text-gray-500 text-sm font-medium line-through">
                                  <CurrencyFormat
                                    value={product.price}
                                    displayType={"text"}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    prefix={"Rp. "}
                                  />
                                </div>
                              </div>
                              <div className="text-orange-600 font-bold text-base ">
                                <CurrencyFormat
                                  value={product.selling_price}
                                  displayType={"text"}
                                  thousandSeparator={'.'}
                                  decimalSeparator={','}
                                  prefix={"Rp. "}
                                />
                              </div>
                            </>
                          ) : (
                            <>
                              <div className="flex">
                                <div className="text-orange-600 font-bold text-base">
                                  <CurrencyFormat
                                    value={product.price}
                                    displayType={"text"}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    prefix={"Rp. "}
                                  />
                                </div>
                              </div>
                            </>
                          )}
                          <div className="flex">
                            <div className="mr-1"><FontAwesomeIcon icon={faStar} className="text-yellow-400"></FontAwesomeIcon></div>
                            <div className="text-sm inline-flex items-center">5.0 | Terjual 40+</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}

                  {isLoading && <div className="text-center text-3xl text-gray-500 flex justify-center items-center w-full mt-3">
                    <div className=" inline-flex items-center animate-spin">
                      <FontAwesomeIcon icon={faCircleNotch}></FontAwesomeIcon>
                    </div>
                  </div>}
                </div>
              </div>
            </div>
          </div>
          {buttonBackToTop && (
            <button onClick={this.scrollToTop} className="w-12 h-12 fixed bottom-8 right-8 bg-orange-700 text-white cursor-pointer rounded-full">
              <FontAwesomeIcon icon={faChevronUp}></FontAwesomeIcon>
            </button>
          )}

          {filterMobile &&
            <div
              style={{ maxHeight: "85%", filter: "drop-shadow(rgba(0, 0, 0, 0.07) 0px 2px 10px) drop-shadow(rgba(0, 0, 0, 0.09) 0px -1px 5px)" }}
              className="fixed md:hidden bottom-0 left-0 right-0 shadow h-fit bg-white z-50 p-3 pt-0 overflow-auto"
            >
              <div className="">

                <div className="">

                  <div className="bg-white font-bold mb-2 text-lg pt-2 flex justify-between">

                    <div>Filter</div>
                    <div
                      className="mx-2 text-gray-500"
                      onClick={() => {
                        this.setState({ filterMobile: false, flagButtonFilter: false })
                      }}
                    ><FontAwesomeIcon icon={faXmark}></FontAwesomeIcon></div>
                  </div>
                </div>
                <div className="">
                  <div className="font-semibold mb-2 pt-3 border-t">Kategory</div>
                  <div className="flex flex-wrap">
                    {category.map((val, idx) => {
                      const status = dataFilter.category.map((e) => { return e.name }).indexOf(val.name) > -1
                      return (
                        <div
                          key={idx}
                          // className="border rounded-full m-1 px-2 py-1"
                          className={
                            status
                              ? "border border-orange-600 rounded-full m-1 px-2 py-1 text-orange-600"
                              : "border rounded-full m-1 px-2 py-1"
                          }
                          onClick={() => {
                            this.setFilterCategory(val)
                            this.setState({ flagButtonFilter: true })
                          }}
                        >{val.name}</div>
                      )
                    })}
                  </div>
                  <div className="font-semibold mb-2 mt-3">Delivery</div>
                  <div className="flex flex-wrap">
                    {delivery.map((valueDelivery, idx) => (
                      <div
                        key={idx}
                        className={dataFilter.delivery.name === valueDelivery.name
                          ? `border border-orange-600 rounded-full m-1 px-2 py-1 text-orange-600` : `border rounded-full m-1 px-2 py-1`}
                        onClick={() => {
                          let initValueDelivery = {}
                          if (this.state.dataFilter.delivery != {}) {
                            if (this.state.dataFilter.delivery.name != valueDelivery.name) {
                              initValueDelivery = valueDelivery
                            }
                          }

                          this.setState({
                            dataFilter: {
                              ...this.state.dataFilter,
                              delivery: initValueDelivery,
                              page: 1
                            },
                            product: [],
                            flagButtonFilter: true
                          });

                          if (initValueDelivery.id == undefined) {
                            initValueDelivery = null
                          } else {
                            initValueDelivery = initValueDelivery.id
                          }

                          const payload = {
                            category: this.state.dataFilter.paramCategory,
                            delivery: initValueDelivery,
                            order: this.state.dataFilter.paramOrder,
                            min: this.state.dataFilter.min,
                            max: this.state.dataFilter.max,
                          }

                          this.getProduct(payload)

                        }}
                      >{valueDelivery.name}</div>
                    ))}
                  </div>
                  <div className="font-semibold mb-2 mt-3">Harga</div>
                  <div className="flex mt-3">
                    <span className="inline-flex items-center px-3 text-sm text-gray-500 font-bold bg-gray-100 rounded-l-md border border-r-0 border-gray-300">
                      Rp
                    </span>
                    <input
                      type="number"
                      ref={this.priceMin}
                      className="hide-arrows rounded-none rounded-r-lg bg-white border text-gray-900 focus:ring-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 focus:border-orange-400 focus:outline-none"
                      placeholder="Harga Minimum"
                      onBlur={(e) => {
                        // if (e.key == "Enter") {}
                        if (parseInt(e.target.value) > 0) {
                          this.setState({
                            dataFilter: {
                              ...this.state.dataFilter,
                              min: parseInt(e.target.value)
                            },
                            product: []
                          });

                          const payload = {
                            category: this.state.dataFilter.paramCategory,
                            delivery: this.state.dataFilter.delivery.id,
                            order: this.state.dataFilter.paramOrder,
                            min: parseInt(e.target.value),
                            max: this.state.dataFilter.max,
                          }

                          this.getProduct(payload)
                        }
                      }}
                    />
                  </div>
                  <div className="flex mt-3">
                    <span className="inline-flex items-center px-3 text-sm text-gray-500 font-bold bg-gray-100 rounded-l-md border border-r-0 border-gray-300">
                      Rp
                    </span>
                    <input
                      type="number"
                      ref={this.priceMax}
                      className="hide-arrows rounded-none rounded-r-lg bg-white border text-gray-900 focus:ring-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 focus:border-orange-400 focus:outline-none"
                      placeholder="Harga Maksimum"
                      onBlur={(e) => {
                        if (parseInt(e.target.value) > 0) {
                          this.setState({
                            dataFilter: {
                              ...this.state.dataFilter,
                              max: parseInt(e.target.value)
                            },
                            product: [],
                          });

                          const payload = {
                            category: this.state.dataFilter.paramCategory,
                            delivery: this.state.dataFilter.delivery.id,
                            order: this.state.dataFilter.paramOrder,
                            min: this.state.dataFilter.min,
                            max: parseInt(e.target.value),
                          }

                          this.getProduct(payload)
                        }
                      }}
                    />
                  </div>

                  <div
                    className="p-3 border font-semibold mt-5 rounded-lg text-center"
                    onClick={() => {
                      this.setState({
                        dataFilter: {
                          page: 1,
                          min: null,
                          max: null,
                          category: [],
                          paramCategory: [],
                          delivery: {},
                          order: "Paling Sesuai"
                        },
                        product: [],
                        pagination: 0,
                        filterMobile: false,
                        flagButtonFilter: false,
                      })

                      this.priceMin.current.value = null
                      this.priceMax.current.value = null

                      this.getProduct()
                    }}
                  >Reset Filter
                  </div>
                  {
                    flagButtonFilter &&
                    <div
                      className="p-3 border font-semibold mt-5 rounded-lg text-center bg-orange-700 text-white"
                      onClick={() => {
                        this.setState({ filterMobile: false, flagButtonFilter: false })
                      }}
                    >Tampilkan {this.state.totalData} Product
                    </div>
                  }
                </div>
              </div>
            </div>
          }

          <div className="hidden md:block">
            <Footer></Footer>
          </div>
        </div>
      </>
    );
  }
}

const mapState = (state) => ({
  listProduct: state.product.product_list,
  filterProduct: state.product.filter,
  pagination: state.product.pagination,
  totalData: state.product.total_data,
});

const mapDispatch = {
  browseCatalog: productAction.browse,
  resetProduct: productAction.resetProduct,
  resetFilter: productAction.resetFilter,
};

export default withRouter(connect(mapState, mapDispatch)(Product));