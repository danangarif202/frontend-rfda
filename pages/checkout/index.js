import Head from 'next/head';
import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import axios from "axios";
import storageKey from "../../src/key/storage.key";
import storagePlugin from "../../src/plugins/storage.plugin";

import { Image, Alert } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faChevronRight, faStar, faEllipsis, faCircleNotch, faCirclePlus, faCircleMinus, faTrashCan, faXmark, faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import CurrencyFormat from "react-currency-format";
import Footer from "../../src/components/footer";

class Cart extends Component {
    state = {
        pengiriman: {
            type: "Pengiriman",
            ekspedisi: []
        },
        activeExpedisi: {},
        activeDwopdown: false,
        activeSelectExpedisi: false,
    };

    componentDidMount() {
        const { router } = this.props;
        const isUser = storagePlugin.isExist(storageKey.storeKey);
        if (!isUser) {
            router.push(`login`);
        } else {
            this.getCart();
        }
    }

    getCart() { };

    setPengiriman = (value) => {
        this.setState({ pengiriman: value, activeDwopdown: false, activeExpedisi: value.ekspedisi[0] })
    };

    goToPayment = () => { };

    render() {
        const { activeDwopdown, pengiriman, activeExpedisi } = this.state;

        const dummyData = [
            {
                id: 2,
                name: "Celana Pendek Waffa Tali Bahan Babyterry - Salem BBTR, All Size",
                price: "53000",
                count: 1,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(9).jpg"
            },
            {
                id: 4,
                name: "Dailyoutfit Exclusive Kaos Zodiac Scorpio Unisex Premium Quality - M, Hitam",
                price: "75000",
                count: 3,
                image: "https://api-rfda.herokuapp.com/uploads/1647958199896-product(11).jpg"
            },
        ]

        const dummyEkspedisi = [
            {
                type: "Next Day",
                ekspedisi: [
                    {
                        name: "JNE",
                        price: "37000",
                        estimate: "Estimasi tiba besok - 26 May",
                        cod: false,
                    }
                ]
            },
            {
                type: "Reguler",
                ekspedisi: [
                    {
                        name: "AnterAja",
                        price: "27000",
                        estimate: "Estimasi tiba 26 - 29 May",
                        cod: true,
                    },
                    {
                        name: "JNE",
                        price: "23000",
                        estimate: "Estimasi tiba 26 - 29 May",
                        cod: false,
                    },
                    {
                        name: "Sicepat Reg",
                        price: "16000",
                        estimate: "Estimasi tiba 26 - 29 May",
                        cod: true,
                    },
                ]
            },
            {
                type: "Kargo",
                ekspedisi: [
                    {
                        name: "JNE",
                        price: "50000",
                        estimate: "Estimasi tiba 26 - 29 May",
                        cod: false,
                    }
                ]
            },
            {
                type: "Ekonomi",
                ekspedisi: [
                    {
                        name: "JNE",
                        price: "17000",
                        estimate: "Estimasi tiba besok - 28 May",
                        cod: false,
                    },
                    {
                        name: "Sicepat Reg",
                        price: "13000",
                        estimate: "Estimasi tiba besok - 28 May",
                        cod: true,
                    },
                ]
            },
        ]

        let totalItem = 0;
        let totalPrice = 0;
        dummyData.map((e) => {
            totalItem += parseInt(e.count);
            totalPrice += parseInt(e.price) * e.count;
        })

        let valueInvoice = 0
        if (activeExpedisi.price != undefined) {
            valueInvoice = parseInt(totalPrice) + parseInt(activeExpedisi.price)
        }

        return (
            <>
                <Head>
                    <title>{"Checkout | RFDA"}</title>
                    <meta property="og:title" content="My page title" key="title" />
                    {/* <link rel="shortcut icon" href="/icon-web.png" /> */}
                </Head>
                <div>
                    <div className="border-b">
                        <div className="container mx-auto py-4 px-28">
                            <div
                                className="text-orange-700 text-2xl font-bold cursor-pointer"
                                onClick={() => {

                                }}
                            >RF|DA</div>
                        </div>
                    </div>
                    <div className="container mx-auto py-4 md:py-8 px-28">
                        <div className="flex">
                            <div className="w-9/12">
                                <div className="mb-4 font-bold text-xl">Checkout</div>
                                <div className="border-b flex">
                                    <div className="mb-3 font-semibold">
                                        Alamat Pengiriman
                                    </div>
                                </div>
                                <div className="border-b py-3">
                                    <div><span className="font-semibold">Danang Arif</span> (RUMAH) Utama</div>
                                    <div className="my-1">6281259198767</div>
                                    <div className="text-sm">Perum Wahyu Taman Sarirogo Blok AF-18 sumput - Sidoarjo</div>
                                    <div className="text-sm">Sidoarjo, Kab. Sidoarjo, 61228</div>
                                </div>
                                <div className="border-b-4 py-3">
                                    <div className="border rounded-lg py-2 px-3 w-fit font-semibold">Pilih Alamat Lain</div>
                                </div>
                                <div className="border-b">
                                    <div className="mb-3">
                                        <div className="font-semibold mt-3">Autozee Jaya Baut Motor</div>
                                        <div className="text-gray-500 text-sm">Jakarta Timur</div>
                                    </div>
                                    <div className="flex mb-3">
                                        <div className="w-7/12">
                                            {dummyData.map((value, idx) => (
                                                <div key={idx} className="mb-3">
                                                    <div className="flex">
                                                        <div className="w-2/12">
                                                            <div className="w-full cursor-pointer">
                                                                <Image
                                                                    className="rounded-lg w-24"
                                                                    preview={false}
                                                                    src={value.image}
                                                                    alt={"sss"}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="w-10/12 pl-2">
                                                            <div>{value.name}</div>
                                                            <div className="text-sm my-1">{value.count} barang ({value.count * 100} gr)</div>
                                                            <div className="font-semibold">
                                                                <CurrencyFormat
                                                                    value={value.price}
                                                                    displayType={"text"}
                                                                    thousandSeparator={'.'}
                                                                    decimalSeparator={','}
                                                                    prefix={"Rp "}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                            )}
                                        </div>
                                        <div className="w-5/12 pl-2">
                                            <div className="text-sm font-semibold">Pilih Durasi</div>
                                            <div className="my-2 relative">
                                                <div
                                                    style={{ paddingLeft: "41%" }}
                                                    className={`rounded-lg cursor-pointer border w-full justify-between text-white bg-orange-600 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-sm px-4 py-2 text-center inline-flex items-center`}
                                                    onClick={() => {
                                                        this.setState({ activeDwopdown: !activeDwopdown })
                                                    }}
                                                >
                                                    {pengiriman.type}
                                                    <FontAwesomeIcon icon={!activeDwopdown ? faChevronDown : faChevronUp}></FontAwesomeIcon>
                                                </div>
                                                <div className="flex justify-end">
                                                    <div className={`${!activeDwopdown ? 'hidden' : ''} border cursor-pointer z-10 w-full bg-white rounded-lg divide-y divide-gray-100 shadow absolute`}>
                                                        <ul className="py-1 text-sm text-gray-600">
                                                            {dummyEkspedisi.map((value, idx) => {
                                                                let arrayPrice = []
                                                                let arrayCod = []
                                                                value.ekspedisi.map((e) => { arrayPrice.push(e.price) })
                                                                value.ekspedisi.map((e) => { arrayCod.push(e.cod) })
                                                                const maxPrice = Math.max(...arrayPrice)
                                                                const minPrice = Math.min(...arrayPrice)
                                                                let rangePrice = ""
                                                                if (minPrice != maxPrice) {
                                                                    rangePrice = <div>
                                                                        <CurrencyFormat
                                                                            value={minPrice}
                                                                            displayType={"text"}
                                                                            thousandSeparator={'.'}
                                                                            decimalSeparator={','}
                                                                            prefix={"Rp "}
                                                                        />
                                                                        <span> - </span>
                                                                        <CurrencyFormat
                                                                            value={maxPrice}
                                                                            displayType={"text"}
                                                                            thousandSeparator={'.'}
                                                                            decimalSeparator={','}
                                                                            prefix={"Rp "}
                                                                        />
                                                                    </div>
                                                                } else {
                                                                    rangePrice = <CurrencyFormat
                                                                        value={maxPrice}
                                                                        displayType={"text"}
                                                                        thousandSeparator={'.'}
                                                                        decimalSeparator={','}
                                                                        prefix={"Rp "}
                                                                    />
                                                                }
                                                                let statusCod = false
                                                                if (arrayCod.indexOf(true) !== -1) {
                                                                    statusCod = true
                                                                }
                                                                return (
                                                                    <li
                                                                        key={idx}
                                                                        onClick={() => {
                                                                            this.setPengiriman(value);
                                                                        }}
                                                                        className={`${idx != dummyEkspedisi.length - 1 ? 'border-b' : ''} p-3`}
                                                                    >
                                                                        <div className="flex justify-between">
                                                                            <div>{value.type}</div>
                                                                            <div>{rangePrice}</div>
                                                                        </div>
                                                                        {statusCod &&
                                                                            <div>{"Tersedia COD (Bayar di Tempat)"}</div>
                                                                        }
                                                                        <div>{value.ekspedisi[0].estimate}</div>
                                                                    </li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            {(pengiriman.type != "Pengiriman") &&
                                                <>
                                                    <div className="text-sm font-semibold mb-2">Kurir Pilihan</div>
                                                    <div className="relative">

                                                        <div className="flex justify-between text-sm mb-1">
                                                            <div>{activeExpedisi.name} (
                                                                <CurrencyFormat
                                                                    value={activeExpedisi.price}
                                                                    displayType={"text"}
                                                                    thousandSeparator={'.'}
                                                                    decimalSeparator={','}
                                                                    prefix={"Rp "}
                                                                />)</div>
                                                            {pengiriman.ekspedisi.length > 1 && <div
                                                                className="text-orange-600 cursor-pointer"
                                                                onClick={() => {
                                                                    this.setState({ activeSelectExpedisi: true })
                                                                }}
                                                            >Ubah</div>}
                                                            {this.state.activeSelectExpedisi && <div className="absolute border rounded-lg bg-white w-full">
                                                                {pengiriman.ekspedisi.map((value, idx) => (
                                                                    <div
                                                                        key={idx}
                                                                        className="border-b p-3"
                                                                        onClick={() => {
                                                                            this.setState({ activeExpedisi: value, activeSelectExpedisi: false })
                                                                        }}
                                                                    >
                                                                        <div className="flex justify-between">
                                                                            <div className="font-semibold">{value.name}</div>
                                                                            <div><CurrencyFormat
                                                                                value={value.price}
                                                                                displayType={"text"}
                                                                                thousandSeparator={'.'}
                                                                                decimalSeparator={','}
                                                                                prefix={"Rp "}
                                                                            /></div>
                                                                        </div>
                                                                        <div className="flex justify-between">
                                                                            <div>
                                                                                <div>{value.estimate}</div>
                                                                                {value.cod &&
                                                                                    <div>{"Tersedia COD (Bayar di Tempat)"}</div>
                                                                                }
                                                                            </div>
                                                                            <div className="inline-flex items-center">{activeExpedisi.name == value.name && <FontAwesomeIcon className="text-xl text-orange-500" icon={faCircleCheck}></FontAwesomeIcon>}</div>
                                                                        </div>
                                                                    </div>
                                                                ))}
                                                            </div>}
                                                        </div>
                                                    </div>
                                                    <div className="text-sm mb-2">{activeExpedisi.estimate}</div>
                                                    <div>Asuransi Pengiriman</div>
                                                </>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="flex justify-between mt-3 font-bold">
                                    <div>Subtotal</div>
                                    <div><CurrencyFormat
                                        value={totalPrice}
                                        displayType={"text"}
                                        thousandSeparator={'.'}
                                        decimalSeparator={','}
                                        prefix={"Rp "}
                                    /></div>
                                </div>
                            </div>
                            <div className="w-3/12">
                                <div className="border rounded-lg ml-6 my-4">
                                    <div className="p-4 border-b-4">
                                        <div className="p-3 border rounded-lg text-gray-500">
                                            <div className="font-bold">Makin hemat pakai promo</div>
                                            <div className="text-sm">Pilih barang dulu sebelum pakai promo</div>
                                        </div>
                                    </div>
                                    <div className="p-4">
                                        <div className="font-bold text-gray-600">Ringkasan belanja</div>
                                        <div className="flex justify-between mt-3 text-gray-500">
                                            <div>Total Harga ({totalItem} produk)</div>
                                            <div>
                                                <CurrencyFormat
                                                    value={totalPrice}
                                                    displayType={"text"}
                                                    thousandSeparator={'.'}
                                                    decimalSeparator={','}
                                                    prefix={"Rp "}
                                                />
                                            </div>
                                        </div>
                                        <div className="flex justify-between text-gray-500">
                                            <div>Total Ongkos Kirim</div>
                                            <div>
                                                {activeExpedisi.price == undefined ?
                                                    "-"
                                                    :
                                                    <CurrencyFormat
                                                        value={activeExpedisi.price}
                                                        displayType={"text"}
                                                        thousandSeparator={'.'}
                                                        decimalSeparator={','}
                                                        prefix={"Rp "}
                                                    />
                                                }
                                            </div>
                                        </div>
                                        <div className="flex justify-between mb-3 pb-4 border-b text-gray-500">
                                            <div>Asuransi Pengiriman</div>
                                            <div>
                                                <CurrencyFormat
                                                    value={0}
                                                    displayType={"text"}
                                                    thousandSeparator={'.'}
                                                    decimalSeparator={','}
                                                    prefix={"Rp "}
                                                />
                                            </div>
                                        </div>
                                        <div>
                                            <div className="flex justify-between my-3 font-bold text-gray-600">
                                                <div>Total Tagihan</div>
                                                <div>
                                                    {
                                                        valueInvoice > 0 ?
                                                            <CurrencyFormat
                                                                value={valueInvoice}
                                                                displayType={"text"}
                                                                thousandSeparator={'.'}
                                                                decimalSeparator={','}
                                                                prefix={"Rp "}
                                                            />
                                                            :
                                                            <>-</>
                                                    }
                                                </div>
                                            </div>
                                            <div className="text-sm">Dengan mengaktifkan asuransi, Saya menyetujui syarat dan ketentuan yang berlaku</div>
                                            <div
                                                style={valueInvoice > 0 ? { backgroundColor: "#03AC0E" } : {}}
                                                className={`${valueInvoice > 0 ? 'text-white cursor-pointer' : 'bg-gray-300 text-gray-500 cursor-not-allowed'} py-2 font-bold border text-center rounded-lg my-2`}
                                                onClick={() => {
                                                    this.goToPayment();
                                                }}
                                            >
                                                Pilih Pembayaran
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer></Footer>
                </div>
            </>

        );
    }
}

export default withRouter(Cart);
