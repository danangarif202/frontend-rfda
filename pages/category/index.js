import { Component } from "react";
import Navbar from "../../src/components/navbar";

import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faStar, faEllipsis, faCircleNotch, faFilter, faXmark } from "@fortawesome/free-solid-svg-icons";

class Category extends Component {
  state = {
    car: [],
    activeCar: {
      id: null,
      tipe: "",
    },
    plat: [],
    activePlat: {
      plat: {
        id: null,
        plat_name: ""
      },
    },
    driver: [],
    activeDriver: {
      id: null,
      name: "",
    },
    dropdownCar: false,
    dropdownPlat: false,
    dropdownDriver: false,
    stopButton: {
      status: false,
      id_data: null
    },
  };

  componentDidMount() {
    this.getCar();
    this.getDriver();
  }

  getCar() {
    axios
      .get(
        `http://localhost:5000/car?perPage=15`
      )
      .then((res) => {
        this.setState({
          car: res.data.data,
        })
      })
      .catch((error) => {
        alert(error);
      });
  }

  getDriver() {
    axios
      .get(
        `http://localhost:5000/driver?perPage=15`
      )
      .then((res) => {
        this.setState({
          driver: res.data.data,
        })
      })
      .catch((error) => {
        alert(error);
      });
  }

  setListPlat(id) {
    this.setState({
      plat: [], activePlat: { plat: { id: null, plat_name: "" } }
    })
    axios
      .get(
        `http://localhost:5000/pivot?car=${id}`
      )
      .then((res) => {
        this.setState({
          plat: res.data.data,
        })
      })
      .catch((error) => {
        alert(error);
      });

    this.setState({ dropdownCar: false })
  }

  startTracking() {
    const { activeCar, activePlat, activeDriver } = this.state;
    if (activePlat.plat.id != null && activeCar.id != null && activeDriver.id != null) {
      const payload = {
        armada_id: activePlat.armada.id,
        driver_id: activeDriver.id,
        plat_id: activePlat.plat.id,
        kendaraan_id: activeCar.id,
      }

      console.log("payload", payload)

      axios.post('http://localhost:5000/tracking', payload)
        .then((res) => {
          this.setState({ stopButton: { status: true, id_data: res.data.data.id } })
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  stopTracking(id) {
    axios.patch(`http://localhost:5000/tracking/${id}`)
      .then((res) => {
        console.log(res);
        this.setState({
          activeCar: {
            id: null,
            tipe: "",
          },
          plat: [],
          activePlat: {
            plat: {
              id: null,
              plat_name: ""
            },
          },
          activeDriver: {
            id: null,
            name: "",
          },
          dropdownCar: false,
          dropdownPlat: false,
          dropdownDriver: false,
          stopButton: {
            status: false,
            id_data: null
          },
        })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { dropdownCar, dropdownPlat, dropdownDriver, car, activeCar, plat, activePlat, driver, activeDriver } = this.state;
    return (
      <>
        <div className="mx-auto pt-24 py-4 md:py-8 text-center h-screen bg-gradient-to-br from-red-200 via-fuchsia-400 to-orange-600">

          <div className="w-8/12 md:w-4/12 mx-auto text-left text-white mb-2 mt-5 font-semibold z-30">Kendaraan</div>
          <div className="relative">
            <div
              className={`${dropdownCar ? 'rounded-t-lg' : 'rounded-lg'} cursor-pointer border w-8/12 md:w-4/12 justify-between text-white focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-sm px-4 py-2.5 text-center inline-flex items-center`}
              onClick={() => {
                this.setState({ dropdownCar: !dropdownCar, dropdownPlat: false, dropdownDriver: false })
              }}
            >
              <div className="w-11/12 text-left">{activeCar.tipe}</div>
              <div className="w-1/12"><FontAwesomeIcon icon={!dropdownCar ? faChevronDown : faChevronUp}></FontAwesomeIcon></div>
            </div>
            <div
              style={{ transform: "translate(-50%, -50%)" }}
              className={`${!dropdownCar ? 'hidden' : ''} cursor-pointer z-10 w-8/12 md:w-4/12  divide-y divide-gray-100 shadow absolute top-full bottom-0 left-1/2`}
            >
              <ul className="py-1 text-sm text-left text-gray-600 bg-white border rounded-b-lg">
                {car.map((value, idx) => (
                  <li
                    key={idx}
                    onClick={() => {
                      this.setState({ activeCar: value })
                      this.setListPlat(value.id);
                    }}
                  >
                    <div className={`block py-2 px-4 hover:bg-gray-100`}>{value.tipe}</div>
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <div className="w-8/12 md:w-4/12 mx-auto text-left text-white mb-2 mt-5 font-semibold">Plat Nomor</div>
          <div className="relative my-4">
            <div
              className={`${dropdownPlat ? 'rounded-t-lg' : 'rounded-lg'} cursor-pointer border w-8/12 md:w-4/12 justify-between text-white focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-sm px-4 py-2.5 text-center inline-flex items-center`}
              onClick={() => {
                if (plat.length > 0) {
                  this.setState({ dropdownPlat: !dropdownPlat, dropdownCar: false, dropdownDriver: false })
                }
              }}
            >
              <div className="w-11/12 text-left">{activePlat.plat.plat_name}</div>
              <div className="w-1/12"><FontAwesomeIcon icon={!dropdownPlat ? faChevronDown : faChevronUp}></FontAwesomeIcon></div>
            </div>
            <div
              style={{ transform: "translate(-50%, -50%)" }}
              className={`${!dropdownPlat ? 'hidden' : ''} cursor-pointer z-10 w-8/12 md:w-4/12  divide-y divide-gray-100 shadow absolute top-full bottom-0 left-1/2`}
            >
              <ul className="py-1 text-sm text-left text-gray-600 bg-white border rounded-b-lg">
                {plat.map((value, idx) => (
                  <li
                    key={idx}
                    onClick={() => {
                      this.setState({ activePlat: value, dropdownPlat: false })
                    }}
                  >
                    <div className={`block py-2 px-4 hover:bg-gray-100`}>{value.plat.plat_name}</div>
                  </li>
                ))}

              </ul>
            </div>
          </div>

          <div className="w-8/12 md:w-4/12 mx-auto text-left text-white mb-2 mt-5 font-semibold">Driver</div>
          <div className="relative">
            <div
              className={`${dropdownDriver ? 'rounded-t-lg' : 'rounded-lg'} cursor-pointer border w-8/12 md:w-4/12 justify-between text-white focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium text-sm px-4 py-2.5 text-center inline-flex items-center`}
              onClick={() => {
                this.setState({ dropdownDriver: !dropdownDriver, dropdownPlat: false, dropdownCar: false })
              }}
            >
              <div className="w-11/12 text-left">{activeDriver.name}</div>
              <div className="w-1/12"><FontAwesomeIcon icon={!dropdownDriver ? faChevronDown : faChevronUp}></FontAwesomeIcon></div>
            </div>
            <div
              style={{ transform: "translate(-50%, -50%)" }}
              className={`${!dropdownDriver ? 'hidden' : ''} cursor-pointer z-10 w-8/12 md:w-4/12 divide-y divide-gray-100 shadow absolute  top-full bottom-0 left-1/2`}
            >
              <ul className="py-1 text-sm text-left text-gray-600 bg-white border rounded-b-lg">
                {driver.map((value, idx) => (
                  <li
                    key={idx}
                    onClick={() => {
                      this.setState({ activeDriver: value, dropdownDriver: false })
                    }}
                  >
                    <div className={`block py-2 px-4 hover:bg-gray-100`}>{value.name}</div>
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <div className="mt-8 w-full text-center">
            {
              !this.state.stopButton.status ?
                <div
                  className={`${(activePlat.plat.id != null && activeCar.id != null && activeDriver.id != null) ? 'border-orange-600 bg-orange-600' : 'border-gray-400 bg-gray-400'} mt-8 border-2 text-white rounded-full w-16 h-16 mx-auto py-auto`}
                  onClick={() => this.startTracking()}
                >
                  <div className="mt-5 font-bold text-sm">START</div>
                </div>
                :
                <div
                  className={`border-orange-500 bg-orange-500 mt-8 border-2 text-white rounded-full w-16 h-16 mx-auto py-auto`}
                  onClick={() => this.stopTracking(this.state.stopButton.id_data)}
                >
                  <div className="mt-5 font-bold text-sm">FINISH</div>
                </div>
            }
          </div>
        </div>
      </>
    );
  }
}

export default Category;
