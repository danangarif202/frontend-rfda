import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { Provider } from "react-redux";
import configureAppStore from "../src/stores/store";
import { Component } from "react";
import { withRouter } from "next/router";
import "../src/styles/globals.css";

config.autoAddCss = false;
const store = configureAppStore();

class MyApp extends Component {
  componentDidMount() {
    const { router } = this.props;

    router.events.on("routeChangeStart", this.handleRouteChange);
    router.events.on("routeChangeComplete", this.handleRouteChangeComplete);
  }

  componentWillUnmount() {
    const { router } = this.props;
    router.events.off("routeChangeStart", this.handleRouteChange);
    router.events.off("routeChangeComplete", this.handleRouteChangeComplete);
  }

  handleRouteChangeComplete = (url) => {
    window.history.scrollRestoration = "manual";

    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };

  handleRouteChange = (url) => { };

  setLoading() {
    return {
      type: loadingType.LOADING_SET_LOADING,
    };
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </>
    );
  }
}

export default withRouter(MyApp);
