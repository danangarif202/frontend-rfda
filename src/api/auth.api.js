import httpPlugin from "../plugins/http.plugin";

const pathURL = `${process.env.NEXT_PUBLIC_MY_BASE_URL}/auth`;

const authApi = {
  async login(cred) {
    try {
      const data = await httpPlugin.post(`${pathURL}/login`, cred);
      return data;
    } catch (error) {
      throw error;
    }
  },
};

export default authApi;
