import httpPlugin from "../plugins/http.plugin";

const config = {
  path: `${process.env.NEXT_PUBLIC_MY_BASE_URL}/products`,
};

const homeApi = {
  async browse(params) {
    try {
      const resp = await httpPlugin.get(config.path, { params });
      return resp.data;
    } catch (error) {
      throw error;
    }
  },
};

export default homeApi;
