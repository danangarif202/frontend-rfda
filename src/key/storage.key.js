const storageKey = {
  storeKey: "RFDA_CLIENT_STORAGE",
  storeToken: "RFDA_CLIENT_STORAGE_TOKEN",
  user: "RFDA_USER",
};

export default storageKey;
