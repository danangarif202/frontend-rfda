const envKey = {
  authService: process.env.NEXT_PUBLIC_AUTH_SERVICE,
  pesananService: process.env.NEXT_PUBLIC_PESANAN_SERVICE,
  fileService: process.env.NEXT_PUBLIC_FILE_SERVICE,
  productService: process.env.NEXT_PUBLIC_PRODUCT_SERVICE,
  promotionService: process.env.NEXT_PUBLIC_PROMOTION_SERVICE,
  configService: process.env.NEXT_PUBLIC_CONFIG_SERVICE,
  transactionService: process.env.NEXT_PUBLIC_TRANSACTION_SERVICE,
  sellerService: process.env.NEXT_PUBLIC_SELLER_SERVICE,
  apiv1: process.env.NEXT_PUBLIC_API_SERVICE,
  apiv2: process.env.NEXT_PUBLIC_API_SERVICE_V2,
};

export default envKey;
