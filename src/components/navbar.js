import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import Head from 'next/head'
import authAction from "../stores/auth/auth.action";
import storageKey from "../key/storage.key";
import storagePlugin from "../plugins/storage.plugin";

import { Image } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass, faBell, faCartShopping, faEnvelope, faArrowLeft, faBars, faArrowRightFromLine } from "@fortawesome/free-solid-svg-icons";

class Navbar extends Component {
  state = {
    active: "Dashboard",
    activeSearch: false,
    activeMenu: false,
    userLogin: false,
    menu: [
      {
        name: "Dashboard",
        route: "/",
      },
      {
        name: "Product",
        route: "/product",
      },
    ]
  };

  componentDidMount() {
    this.pageURL();
    const isUser = storagePlugin.isExist(storageKey.storeKey);
    this.setState({ userLogin: isUser })
  }

  pageURL() {
    if (window.location.pathname.includes("/product")) {
      this.setState({ active: "Product" });
    } else if (window.location.pathname.includes("/category")) {
      this.setState({ active: "Category" });
    } else if (window.location.pathname.includes("/user")) {
      this.setState({ active: "User" });
    } else if (window.location.pathname.includes("/cart")) {
      this.setState({ active: "Cart" });
    }
  }

  render() {
    const { router, callback, dataAuth, logout } = this.props;
    const { active, menu, activeSearch, activeMenu, userLogin } = this.state;

    // console.log("dataAuth", dataAuth)
    // console.log("userLogin", userLogin)

    return (
      <>
        <Head>
          <title>{active == "Dashboard" ? "Situs Jual Beli Terpercaya | RFDA" : active + " | RFDA"}</title>
          <meta property="og:title" content="My page title" key="title" />
          {/* <link rel="shortcut icon" href="/icon-web.png" /> */}
        </Head>
        <div className="relative">
          <nav className="fixed top-0 left-0 right-0 shadow bg-white z-50">
            <div className="hidden md:block container mx-auto px-2">
              <div className="relative flex items-center justify-between h-16 my-2">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden"></div>
                <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                  <nav className="text-orange-700 text-2xl font-bold">
                    <div>RF|DA</div>
                  </nav>
                  <div className="hidden sm:block sm:ml-6 w-full">
                    <div className="flex space-x-4">
                      {menu.map((menu, idx) => (
                        <div key={idx}>
                          <div
                            className={
                              active == menu.name
                                ? "border-b-4 border-orange-700 text-orange-700 px-3 py-2 text-sm font-bold cursor-pointer"
                                : "text-gray-800 hover:text-gray-500 hover:border-gray-500 hover:border-b-4 px-3 py-2 text-sm font-medium cursor-pointer"
                            }
                            onClick={() => {
                              router.push(menu.route);
                              this.setState({ active: menu.name });
                            }}
                          >
                            {menu.name}
                          </div>
                        </div>
                      ))}
                      <div className="mx-3 w-full my-auto">
                        <div className="relative">
                          <div className="input-group relative flex">
                            <input
                              type="text"
                              className="form-control relative flex-auto min-w-0 block w-full px-3 py-1 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-200 rounded-l-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-orange-400 focus:outline-none"
                              placeholder="Search"
                              onFocus={() => {
                                this.setState({ activeSearch: !activeSearch });
                                callback(true)
                              }}
                              onBlur={() => {
                                this.setState({ activeSearch: !activeSearch });
                                callback(false)
                              }}
                            />
                            <button
                              className="btn px-2 py-2 bg-gray-200 text-gray-400 font-medium text-xs leading-tight uppercase rounded-r-lg hover:bg-gray-700 hover:shadow-lg focus:bg-gray-700  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out flex items-center"
                              type="button"
                            >
                              <FontAwesomeIcon icon={faMagnifyingGlass}></FontAwesomeIcon>
                            </button>
                          </div>
                          {activeSearch &&
                            <div className="border absolute h-80 bg-white rounded-lg p-3 overflow-y-scroll w-full">
                              <div>search data</div>
                            </div>
                          }
                        </div>

                        <div className="flex text-sm mx-3">
                          {["Baju Muslim Pria", "Iphone 12 Mini", "Casing Pc", "Botol Minum", "Toples Kue Kering"].map((kategori, idx) => (
                            <div key={idx} className="mx-2 mt-1 text-gray-500">{kategori}</div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                  <div
                    className="p-3 flex relative rounded-full text-gray-500 hover:text-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white cursor-pointer"
                    onClick={() => {
                      router.push(`/cart`);
                    }}
                  >
                    {userLogin && <div className="absolute text-xs w-5 h-5 bg-orange-600 border-2 text-white rounded-full text-center top-0 right-0">4</div>}
                    <FontAwesomeIcon icon={faCartShopping}></FontAwesomeIcon>
                  </div>
                  <div
                    className="p-1 mx-2 rounded-full text-gray-500 hover:text-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                  >
                    <FontAwesomeIcon icon={faBell}></FontAwesomeIcon>
                  </div>
                  <div
                    className="p-1 mx-2 rounded-full text-gray-500 hover:text-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                  >
                    <FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon>
                  </div>

                </div>
                <div
                  onMouseOver={() => { if (userLogin) { this.setState({ activeMenu: true }) } }}
                  onMouseOut={() => { if (userLogin) { this.setState({ activeMenu: false }) } }}
                  className="ml-3 relative"
                >
                  {!userLogin ?
                    <div className="flex">
                      <div
                        className="border-2 text-orange-600 border-orange-600 p-1 px-3 mr-2 rounded-lg font-bold cursor-pointer"
                        onClick={() => {
                          router.push(`/login`);
                        }}
                      >
                        Masuk
                      </div>
                      <div
                        className="bg-orange-600 text-white p-1 px-3 mr-2 rounded-lg font-bold cursor-pointer"
                        onClick={() => {
                          router.push(`/register`);
                        }}
                      >
                        Daftar
                      </div>
                    </div>
                    :
                    <>
                      <div className="flex cursor-pointer px-2 hover:bg-gray-100 rounded-lg p-1">
                        <div
                          className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                        >
                          <Image
                            className="h-8 w-8 rounded-full"
                            preview={false}
                            src={dataAuth?.data.imageProfile}
                            alt="dsfsd"
                          />
                        </div>
                        <div className="ml-3 text-sm inline-flex items-center capitalize w-24">
                          {dataAuth?.data.name}
                        </div>
                      </div>
                      {
                        activeMenu &&
                        <div className="border p-2 rounded-lg text-sm text-gray-500 absolute bg-white w-full cursor-pointer">
                          {[
                            { label: "Profile", route: "/user" },
                            { label: "Pembelian", route: "/user" },
                            { label: "Wishlist", route: "/user" },
                            { label: "Toko Favorit", route: "/user" },
                            { label: "Pengaturan", route: "/user" },
                          ].map((val, idx) => (
                            <div
                              key={idx}
                              className="hover:text-gray-700 hover:font-semibold py-1"
                              onClick={() => {
                                router.push(val.route)
                              }}
                            >{val.label}</div>
                          ))}
                          <div
                            className="hover:text-gray-700 hover:font-semibold border-t py-1 flex justify-between content-center pr-2"
                            onClick={() => {
                              logout(() => {
                                window.location.reload();
                              });
                            }}
                          >
                            <div>Logout</div>
                            <div><FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon></div>
                          </div>
                        </div>
                      }
                    </>
                  }
                </div>
              </div>
            </div>

            <div className="sm:hidden" id="mobile-menu">
              <div className="p-1 my-2 flex items-center">
                {active != "Dashboard" &&
                  <div
                    className="w-1/12 text-center text-gray-500"
                    onClick={() => {
                      router.back();
                    }}
                  ><FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon></div>
                }
                <div className={`${active == "Dashboard" ? 'w-7/12' : 'w-9/12'}`} >
                  <div className="input-group relative flex mx-1">
                    <input
                      type="text"
                      className="form-control relative flex-auto min-w-0 block w-full px-3 py-1 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-200 rounded-l-lg transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-orange-400 focus:outline-none"
                      placeholder="Search mobile"
                      onFocus={() => {
                        this.setState({ activeSearch: !activeSearch });
                        callback(true)
                      }}
                      onBlur={() => {
                        this.setState({ activeSearch: !activeSearch });
                        callback(false)
                      }}
                    />
                    <button
                      className="btn px-2 py-2 bg-gray-200 text-gray-400 font-medium text-xs leading-tight uppercase rounded-r-lg hover:bg-gray-700 hover:shadow-lg focus:bg-gray-700  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out flex items-center"
                      type="button"
                    >
                      <FontAwesomeIcon icon={faMagnifyingGlass}></FontAwesomeIcon>
                    </button>
                  </div>
                </div>
                <div className={`${active == "Dashboard" ? 'w-5/12' : 'w-2/12'} flex text-center text-gray-500`}>
                  {active != "Dashboard" ?
                    <>
                      <div
                        className="w-1/2"
                        onClick={() => {
                          router.push(`/cart`);
                        }}

                      ><FontAwesomeIcon icon={faCartShopping}></FontAwesomeIcon></div>
                      <div className="w-1/2"><FontAwesomeIcon icon={faBars}></FontAwesomeIcon></div>
                    </>
                    :
                    <>
                      <div className="w-1/4"><FontAwesomeIcon icon={faEnvelope}></FontAwesomeIcon></div>
                      <div className="w-1/4"><FontAwesomeIcon icon={faBell}></FontAwesomeIcon></div>
                      <div
                        className="w-1/4"
                        onClick={() => {
                          router.push(`/cart`);
                        }}
                      ><FontAwesomeIcon icon={faCartShopping}></FontAwesomeIcon></div>
                      <div className="w-1/4"><FontAwesomeIcon icon={faBars}></FontAwesomeIcon></div>
                    </>
                  }
                </div>
              </div>
              {/* <div>
                <div>aaa</div>
                <div>aaa</div>
                <div>aaa</div>
                <div>aaa</div>
                <div>aaa</div>
                <div>aaa</div>
              </div> */}
            </div>
          </nav>
        </div >
      </>
    );
  }
}

const mapState = (state) => ({
  dataAuth: state.auth.credential,
});

const mapDispatch = {
  logout: authAction.logout,
};

export default withRouter(connect(mapState, mapDispatch)(Navbar));