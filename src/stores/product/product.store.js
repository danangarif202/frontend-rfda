import productType from "./product.type";

const initState = {
  filter: {
    page: 1,
    min: null,
    max: null,
    category: [],
    param_category: [],
    delivery: {},
    order: "Paling Sesuai",
    param_order: null
  },
  product_list: [],
  pagination: 0,
  total_data: 0,
};

function productStore(state = initState, actions) {
  const { type, payload } = actions;

  const countData = Math.ceil(payload?.total_data / 15);
  let dataPagination = [];

  for (let i = 0; i < countData; i++) {
    dataPagination.push(i);
  }

  switch (type) {
    case productType.PRODUCT_BROWSE:
      return {
        ...state,
        product_list: [...state.product_list, ...payload.data],
        total_data: payload.total_data,
        pagination: dataPagination.length,
      };

    case productType.PRODUCT_RESET_CATALOG:
      return {
        ...state,
        product_list: []
      };

    case productType.PRODUCT_RESET_FILTER:
      return {
        ...state,
        filter: {}
      };

    default:
      return state;
  }
}

export default productStore;
