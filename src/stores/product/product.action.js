import productType from "./product.type";
import storePlugin from "../../plugins/store.plugin";
import productApi from "../../api/product.api";

const productAction = {
  browse(params) {
    return async (dispatch) => {
      storePlugin.action(async () => {
        const data = await productApi.browse(params);
        dispatch({ type: productType.PRODUCT_BROWSE, payload: data });
      }, dispatch);
    };
  },

  filterProduct(param) {
    console.log("param-filter", param)
    if (param.genders != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_GENDER, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.categories != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_CATEGORY, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.colors != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_COLOR, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.brands != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_BRAND, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.genders != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_GENDER, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }
    if (param.ocasions != undefined) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_OCCASSION, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.max != 0) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_MAX, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

    if (param.min != 0) {
      return (dispatch) => {
        storePlugin.action(
          async () => {
            dispatch({ type: productType.PRODUCT_FILTER_MIN, payload: param });
          },
          (err) => {
            // Handle error
          }
        );
      }
    }

  },

  resetFilter() {
    return (dispatch) => {
      dispatch({ type: productType.PRODUCT_RESET_FILTER });
    };
  },

  resetProduct() {
    return (dispatch) => {
      dispatch({ type: productType.PRODUCT_RESET_CATALOG });
    };
  },
};

export default productAction;
