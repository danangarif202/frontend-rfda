import { combineReducers } from "redux";
import productStore from "./product/product.store";
import authStore from "./auth/auth.store";

export default combineReducers({
  product: productStore,
  auth: authStore,
});
