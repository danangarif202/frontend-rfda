
const storePlugin = {
  async action(callback, dispatch, onError = () => { }) {
    try {
      await callback();
    } catch (error) {
      console.log("error", error.message);
      onError(error);
    }
  },
};

export default storePlugin;
