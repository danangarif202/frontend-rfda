/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    NEXT_PUBLIC_MY_BASE_URL: 'https://api-rfda.herokuapp.com',
  }
}

module.exports = nextConfig
